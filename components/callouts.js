module.exports = (theme) =>  {
    let callouts = {},
        callout = {
            margin: '1rem',
            padding: '.5rem 1rem',
            borderRadius: '.25rem',
            fontSize: '1rem',
            backgroundColor: theme('colors.white'),
            boxShadow: '0 .25rem .25rem rgba(50, 50, 50, 0.2)',
            borderLeft: '4px solid',
        };

    /**
     *  Default button
     */
    callouts['.callout'] = Object.assign({
        borderLeftColor: theme('colors.gray.800'),
        color: theme('colors.gray.800'),
    }, callout)

    /**
     * Theme color generated buttons
     */
    let lightness = {
        'light': 400,
        '': 600,
        'dark': 800,
    };
    for (const color in theme('colors')) {
        for (const l in lightness) {
            let colorStrength = lightness[l]

            if(typeof theme('colors.' + color + '.' + colorStrength) !== 'undefined') {
                callouts['.callout-' + color + (l === '' ? '' : '-' + l)] = Object.assign({
                    borderLeftColor: theme('colors.' + color + '.' + colorStrength),
                    color: theme('colors.' + color + '.' + colorStrength)
                }, callout)
            }
        }
    }

    return callouts
};
