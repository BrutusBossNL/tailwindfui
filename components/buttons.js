module.exports = (theme) =>  {
    let buttons = {},
        button = {
            padding: '.5rem 1rem',
            borderRadius: '.25rem',
            fontWeight: '600',
            fontSize: '1rem',
            textAlign: 'center',
            transitionProperty: 'background-color, border-color, color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter, -webkit-backdrop-filter',
            transitionTimingFunction: 'cubic-bezier(0.4, 0, 1, 1)',
            transitionDuration: '200ms',
            boxShadow: 'var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow)',
            borderStyle: 'solid',
            borderWidth: '1px',
            '&:focus': {
                outline: '2px solid transparent',
                outlineOffset: '2px',
                '--tw-ring-offset-width': '2px',
                '--tw-ring-offset-shadow': 'var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)',
                '--tw-ring-shadow': 'var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color)',
                'box-shadow': 'var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000)',
            }
        };

    /**
     *  Default button
     */
    buttons['.btn'] = Object.assign({
        backgroundColor: theme('colors.gray.200'),
        borderColor: theme('colors.gray.800'),
        color: theme('colors.gray.800'),
        '&:focus': {
            '--tw-ring-color': theme('colors.gray.700'),
        },
        '&:hover': {
            backgroundColor: theme('colors.gray.300'),
        }
    }, button)

    /**
     * Theme color generated buttons
     */
    let lightness = {
        'light': 300,
        '': 500,
        'dark': 700,
    };
    for (const color in theme('colors')) {
        for (const l in lightness) {
            let colorStrength = lightness[l],
                colorText = l === 'light' ? 'black' : 'white'

            if(typeof theme('colors.' + color + '.' + colorStrength) !== 'undefined') {
                buttons['.btn-' + color + (l === '' ? '' : '-' + l)] = Object.assign({
                    backgroundColor: theme('colors.' + color + '.' + colorStrength),
                    borderColor: theme('colors.' + color + '.' + (colorStrength + 100)),
                    color: theme('colors.' + colorText),
                    '&:focus': {
                        '--tw-ring-color': theme('colors.' + color + '.' + (colorStrength - 100)),
                    },
                    '&:hover': {
                        backgroundColor: theme('colors.' + color + '.' + (colorStrength + 100)),
                    }
                }, button)
            }
        }
    }

    return buttons
};
