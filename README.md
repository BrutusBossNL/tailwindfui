#Tailwind CSS - Free User Interface

An open source project free to use and collaborate. This package creates components to be used with Tailwind CSS.

Source code managed via GitLab: https://gitlab.com/BrutusBossNL/tailwindfui

<img src="https://badgen.net/gitlab/last-commit/BrutusBossNL/tailwindfui" alt="commit">
<img src="https://badgen.net/npm/license/tailwindfui" alt="license">
<img src="https://badgen.net/npm/v/tailwindfui" alt="version">

##Installation

1. Install the package using:  
`npm install tailwindfui`  
2. In your `tailwind.config.js` add the following to your plugins array:  
   ```javascript
   module.exports = {
       plugins: [
           require('tailwindfui/plugin'),
       ],
   };
   ```
3. That's it!

##Usage

All components use the declared colors in the theme if available.  
**note:** *when declaring custom colors in the `tailwind.config.js` declare using the default color palette: `50, 100, 200 ... 900` to make use of the different lightness's of a component (if any)*

Currently, the following components are available:

### Buttons

*all active theme colors implemented*  
**classname**: `.btn` (*default gray*)  
**example button**: `.btn-blue` / `.btn-blue-light` / `.btn-blue-dark`  
**example custom color button**: `.btn-kinda-red` or (when used default palette declaration) `.btn-kinda-red-light` / `.btn-kinda-red-dark`

### Callouts

*all active theme colors implemented*  
**classname**: `.callout` (*default gray*)  
**example callout**: `.callout-red` / `.callout-red-light` / `.callout-red-dark`  
**example custom color callout**: `.callout-warning` or (when used default palette declaration) `.callout-warning-light` / `.callout-warning-dark`