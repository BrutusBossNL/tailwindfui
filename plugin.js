const _ = require('lodash')
const plugin = require('tailwindcss/plugin')
const buttons = require('./components/buttons')
const callouts = require('./components/callouts')

module.exports = plugin(function({ addComponents, theme }) {
    addComponents(buttons(theme), ['responsive', 'hover'])
    addComponents(callouts(theme), ['responsive'])
})

